import React from "react";
import products from "../../data/products";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

export default function Product({ addProduct }) {
  return (
    <div className="product">
      {products.map((product, id) => (
        <div key={id} className="card">
          <Card sx={{ maxWidth: 345 }}>
            <CardActionArea>
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  {product.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  € {product.value}{" "}
                </Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </div>
      ))}
      {/*  <button className="btn btn-success" onClick={() => addProduct(product.id)}>
        Add to cart
      </button> */}
    </div>
  );
}
