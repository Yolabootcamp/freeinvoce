import React from "react";
import "./Footer.css";

export default function Footer() {
  const year = new Date().getFullYear();
  return (
    <div>
      <footer>
        <p>© Copyright {year}.</p>
        <div className="footer">
          <p>Created by:</p>
          <p>Jhonny</p> <p>Luis </p> <p>Yolanda</p>
        </div>{" "}
      </footer>
    </div>
  );
}
