import React, { useState } from "react";
import { Redirect } from "react-router";

import "./Login.css";
import access from "./server";

const userAccess = access.user;
const passwordAccess = access.password;

export default function Login() {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [redirect, setRedirect] = useState(false);

  const handleClick = () => {
    userName === userAccess && password === passwordAccess
      ? setRedirect({ redirect: true })
      : alert("User not found. Try again");
  };

  if (redirect) {
    return <Redirect to="/homepage" />;
  }

  return (
    <div className="login-wrapper">
      <div className="container">
        <div className="username">
          <p>Username</p>
          <input
            placeholder="Your user"
            required
            type="text"
            value={userName}
            onChange={(event) => {
              setUserName(event.target.value);
            }}
          />
        </div>
        <div className="password">
          <p>Password</p>
          <input
            placeholder="Type your password"
            required
            type="password"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />
        </div>
      </div>
      <div className="button">
        <button type="submit" onClick={handleClick}>
          Login
        </button>
      </div>
    </div>
  );
}
