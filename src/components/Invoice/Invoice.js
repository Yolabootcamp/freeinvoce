import React, {useState, useEffect} from "react";
import Axios from 'axios';
import './Invoice.css'
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {
  CardActionArea,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';



function Invoice(props) {

  const [getInfo, setGetInfo] = useState([]);

 
  useEffect(() => {
    Axios.get('https://mocki.io/v1/2eac9e22-e646-4bf0-8c94-b4baec413909').then(
      (res) => {
        setGetInfo(res.data);
      }
    );
  },[setGetInfo]);



  return (
    <div className="Invoice">
      <Card sx={{ maxWidth: 375 }}>
        <CardActionArea>
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
            Invoice #{Math.floor(Math.random() * 100)}
            {/* {getInfo.length > 0 && getInfo[props.index].number} */}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>Products</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography>
                  {getInfo.length > 0 && getInfo[props.index].products}
                  </Typography>
                </AccordionDetails>
              </Accordion>
              
            </Typography>
            <div className="Total">
              <Typography>Total: €{getInfo.length > 0 && getInfo[props.index].total}</Typography>
            </div>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
}

export default Invoice;