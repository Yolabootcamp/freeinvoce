import React from "react";
import "./HomePage.css";
import Invoice from "../Invoice/Invoice";
import invoices from "../../data/invoices";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";

function HomePage() {
  return (
    <div>
      <h4>Your Invoices</h4>
      {invoices.map((newInvoice, index) => (
        <Invoice
          index={index}
          total={newInvoice.total}
          number={newInvoice.number}
          products={newInvoice.products}
          name={newInvoice.name}
        />
      ))}
      <div className="FabIcon">
        <Fab color="primary" aria-label="add">
          <AddIcon />
        </Fab>
      </div>
    </div>
  );
}

export default HomePage;
