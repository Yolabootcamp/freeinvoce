import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import "./NavBar.css";

export default function NavBar() {
  const [burguerOpen, setBurgerOpen] = useState(false);

  const toggleBurguer = () => {
    setBurgerOpen(!burguerOpen);
  };

  return (
    <div className="main-container">
      <nav
        className={burguerOpen ? "icon iconActive" : "icon"}
        onClick={toggleBurguer}
      >
        <div className="hamburguer hamburguerIcon"> </div>
        <div className={burguerOpen ? "menu menuOpen" : "menu menuClose"}>
          <div className="list">
            <ul className="listItems">
              <li>
                <NavLink exaxt to="/">
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink to="/profile">Profile</NavLink>
              </li>
              <li>
                <NavLink to="/invoices">Invoices</NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
}
