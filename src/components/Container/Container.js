import React, { useState } from "react";
import CreateInvoice from "../CreateInvoice/CreateInvoice";

export default function Container() {
  const [invoices, setInvoices] = useState([]);

  const addInvoice = (newInvoice) => {
    setInvoices((prevInvoice) => {
      return [...prevInvoice, newInvoice];
    });
  };
  return (
    <div>
      <CreateInvoice addInvoice={addInvoice} />
          </div>
  );
}
/* {invoices.map((invoice, index) => {
        return (
          <CreateInvoice
            key={index}
            id={index}
            number={invoice.number}
            userName={invoice.userName}
            total={invoice.total}
            products={invoice.products}
          />
        );
      })} */