import React, { useState, useEffect } from "react";
import axios from "axios";

export default function UserProfile() {
  //const [userName, setUserName] = useState("");
  //const [userChosen, setUserChosen] = useState(false);
  const [userData, setUserData] = useState([
    {
      id: "",
      name: "",
      password: "",
      age: "",
      invoices: [
        {
          id: "",
          number: "",
          total: "",
          iname: "",
          products: [
            {
              id: "",
              name: "",
              value: "",
            },
          ],
        },
      ],
    },
  ]);

   /* useEffect(() => {
    getUser();
  }, []);

  
  cons getUser = async()=>{ 
  axios.get("http://localhost:8080/users").then((response) => {
    setUserData([
      {
        id: response.data.id,
        name: response.data.name,
        password: response.data.password,
        age: response.data.age,
        invoices: [
          {
            id: response.data.invoices[0].id,
            number: response.data.invoices[1].number,
            total: response.data.invoices[2].total,
            name: response.data.invoices[3].name,
            products: [
              {
                id: response.data.produc[0].id,
                name: response.data.product[0].name,
                value: response.data.invoices[0].value,
              },
            ],
          },
        ],
      },
    ]);
  });
 } */
  return (
    <div>
      <div>
        {userData.map((user) => {
          return (
            <div key={user.id}>
              <p>User name: {user.name}</p>
              <p>Password: {user.password}</p>
              <p>Age: {user.age}</p>
              <p>
                Invoices list:<li>{user.invoices}</li>
              </p>
              <p>
                Products list: <li>{user.products}</li>{" "}
              </p>
            </div>
          );
        })}
      </div>
    </div>
  );
}
