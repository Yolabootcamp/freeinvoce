import React from "react";
import "./Header.css";
import NavBar from "../NavBar/NavBar";

export default function Header() {
  return (
    <header>
      <h1 className="header-title">FreeInvoice</h1>
      <NavBar />
    </header>
  );
}
