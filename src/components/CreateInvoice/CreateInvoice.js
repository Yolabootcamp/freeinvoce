import React, { useState } from "react";
import invoices from "../../data/invoices";
import products from "../../data/products";
import Product from "../Product/Product";

export default function CreateInvoices({ addInvoice }) {
  const [insertUser, setInsertUser] = useState("");
  const [addProduct, setAddProduct] = useState([]);
  const [qty, setQty] = useState(1);

  const [createInvoice, setCreateInvoice] = useState([
    {
      userName: "",
      invoice: [{}],
      products: [{}],
    },
  ]);

  const handleSaveName = (event) => {
    event.preventDefault();
    setInsertUser(event.target.value);
  };

  /*   
const handleAdd = () => {
  
    {products.map(product =>(
      //  if(product.id === )
      setAddProduct(addProduct.push(product.id));
    ))}
  }; */

  const handleSave = () => {
    setCreateInvoice({
      userName: insertUser,
      invoice: [invoices],
      products: addProduct,
    });
  };

  return (
    <div>
      <h1>Create an Invoice</h1>
      <form>
        <h5>Insert client name: </h5>
        <input
          type="text"
          value={insertUser}
          placeholder="Name"
          onChange={(event) => {
            setInsertUser(event.target.value);
          }}
        />
        <button onClick={handleSaveName}> OK</button>
        <h5>Select product: </h5>
        <div>
          <select value={qty} onChange={(event) => setQty(event.target.value)}>
            {products.map((product, id) => (
              <option key={id} value={product.id}>
                {" "}
                {product.name}
              </option>
            ))}
          </select>
          <button onClick={handleAdd}>Add</button>
        </div>
        <button onClick={handleSave}>Save invoice</button>
      </form>

      <div>
        {createInvoice.map((list) => {
          return (
            <div key={list.id}>
              <p>{list.userName}</p>
              <p>{list.invoice}</p>
              <p>{list.products}</p>
            </div>
          );
        })}
      </div>
      <Product addProduct={addProduct} />
    </div>
  );
}
