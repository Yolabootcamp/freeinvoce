import "./App.css";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Login from "./components/loginPage/Login";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./components/homepage/HomePage";

function App() {
  return (
    <>
      <div className="wrapper">
        <BrowserRouter>
          <Header />
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/homepage" component={HomePage} />
          </Switch>
          <Footer />
        </BrowserRouter>
      </div>
    </>
  );
}
export default App;
