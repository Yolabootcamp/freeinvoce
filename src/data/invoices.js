

const invoices = [
  {
    id: 1,
    number: Math.floor(Math.random() * 100),
    products: "Macbook",
    total: 2000,
    name: "Maria",
  },
  {
    id: 2,
    number: Math.floor(Math.random() * 100),
    products: "iPhone",
    total: 1200,
    name: "Maria",
  },
  {
    id: 3,
    number: Math.floor(Math.random() * 100),
    products: "AirPods",
    total: 210,
    name: "Maria",
  },
  {
    id: 4,
    number: Math.floor(Math.random() * 100),
    products: "iPad",
    total: 1700,
    name: "Maria",
  },
  {
    id: 5,
    number: Math.floor(Math.random() * 100),
    products: "iPod",
    total: 1250,
    name: "Maria",
  },
  {
    id: 6,
    number: Math.floor(Math.random() * 100),
    products: "airTags",
    total: 40,
    name: "Maria",
  },
  {
    id: 7,
    number: Math.floor(Math.random() * 100),
    products: "Apple Watch",
    total: 500,
    name: "Maria",
  },
  {
    id: 8,
    number: Math.floor(Math.random() * 100),
    products: "Padel Racket",
    total: 600,
    name: "Maria",
  },
  {
    id: 9,
    number: Math.floor(Math.random() * 100),
    products: "Football Boots",
    total: 450,
    name: "Maria",
  },
  {
    id: 10,
    number: Math.floor(Math.random() * 100),
    products: "Concert Tickets",
    total: 400,
    name: "Maria",
  },
  {
    id: 11,
    number: Math.floor(Math.random() * 100),
    products: "Playstation 5",
    total: 700,
    name: "Maria",
  },
];

export default invoices;
