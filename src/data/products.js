const products = [
  {
    id: "1",
    name: "Macbook",
    value: 1500,
  },
  {
    id: "2",
    name: "Prada Bag",
    value: 500,
  },
  {
    id: "3",
    name: "Nike Air Force 1",
    value: 300,
  },
  {
    id: "4",
    name: "Gucci Sandals",
    value: 500,
  },
];

export default products;
